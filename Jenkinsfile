// Define the binaries to build.
def BINARIES = ['pcas-mongoindex', 'pcas-mongosentry', 'pcas-mongoshard']

// Define the architectures to build on.
def TARGETS = ['linux-amd64', 'darwin-arm64', 'darwin-amd64']

// When we loop over binaries and architectures below, we do using basic for loops with a single integer index. There are more idiomatic Groovy looping constructions, but according to the internet they sometimes fail in the Jenkins dialog of Groovy.

// Refspec for the Jenkins job:             +refs/tags/v*:refs/remotes/origin/tags/v*
// Branch specifier for the Jenkins job:    **/tags/v*

// Note that we don't need a "when" condition to check if we are building a tag here, because we are not in fact building a tag. The refspec in the Jenkins job copies the tag "vX.Y.Z" across as a new branch origin/tags/vX.Y.Z, and we are building that branch.

pipeline {
    agent { 
        docker { 
            image 'focal-go' 
            args '-v /var/jenkins:/output'
        } 
    }
    environment {
        // Make sure we can write to the Go module cache even though we are not running as root
        XDG_CACHE_HOME = '/tmp/.cache'
    }
    stages {
        stage('Build') {
            steps {
                script {
                    // Loop over the binaries
                    for (int i = 0; i < BINARIES.size(); i++) {
                        bin = BINARIES[i]
                        sh """
                            cd cmd/${bin}
                            make release
                        """
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                script {
                    // Grab the tag name
                    tag = sh(returnStdout: true, script: "git tag --sort version:refname | tail -1").trim()
                    // Move the binaries into place
                    for (int i = 0; i < BINARIES.size(); i++) {
                        bin = BINARIES[i]
                        // Move the binaries into /var/jenkins on the host machine
                        for (int j = 0; j < TARGETS.size(); j++) {
                            arch = TARGETS[j]
                            source_dir = "cmd/${bin}/release/${arch}"
                            target_dir = "/output/website/static/${bin}/${tag}/${arch}"
                            sh """
                                mkdir -p ${target_dir}
                                mv ${source_dir}/${bin}-${tag}.gz ${target_dir}/${bin}.gz
                                mv ${source_dir}/sha256 ${target_dir}/sha256
                            """
                        }
                        // Write out the YAML file that Hugo will use to find the new version
                        fileSpec = [
                            'title': bin,
                            'commit': GIT_COMMIT,
                            'tag' : tag,
                            'architectures': TARGETS,
                            'draft': false
                        ]
                        yamlDir = "/output/website/content/en/downloads/specs"
                        yamlFile = "${yamlDir}/${bin}.md"
                        yamlOutput = writeYaml returnText: true, data: fileSpec
                        sh "mkdir -p ${yamlDir}"
                        sh "cat >${yamlFile} << EOF\n---\n${yamlOutput}---\nEOF"
                    }
                }
            }
        }
    }
}

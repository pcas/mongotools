// Pcas-mongoshard shards MongoDB collections

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/mongotools"
	"fmt"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// progressMeter displays a progress meter.  To stop this, close done.  progress_meter closes c just before it exits.  Designed to be run in its own goroutine.
func progressMeter(c chan struct{}, done chan struct{}) {
	fmt.Printf("Sharding collection")
	ticker := time.NewTicker(1 * time.Second)
	i := 0
	ok := true
	for ok {
		select {
		case <-ticker.C:
			if i == 3 {
				fmt.Printf("\b\b\b")
				i = 0
			} else {
				fmt.Printf(".")
				i++
			}
		case <-done:
			ok = false
			for j := i; j < 3; j++ {
				fmt.Printf(".")
			}
			fmt.Printf("done\n")
		}
	}
	// Close c to signal that we are exiting, and then exit
	close(c)
	return
}

// shardCollection shards the specified collection in the specified database, using hashed sharding on the specified key
func shardCollection(S *mgo.Session, database string, collection string, key string) error {
	// enable sharding on the database
	cmd := bson.M{"enableSharding": database}
	result := make(map[string]string)
	if err := mongotools.RunCommandAndUnmarshal(S, "admin", cmd, result); err != nil {
		return errors.Wrapf(err, "Error enabling sharding.  The MongoDB server returned %v", result)
	}
	// shard the collection
	cmd = bson.M{
		"shardCollection": database + "." + collection,
		"key":             bson.M{key: "hashed"},
	}
	if err := mongotools.RunCommandAndUnmarshal(S, "admin", cmd, result); err != nil {
		return errors.Wrapf(err, "Error sharding collection.  The MongoDB server returned %v", result)
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// main
func main() {
	// Parse the options
	opts := setOptions()
	// Dial the MongoDB server
	S, err := mgo.DialWithTimeout(opts.Address.URI(), 10*time.Second)
	assertNoErr(err)
	// Check that the database and collection exist
	if !opts.Create {
		_, _, err = mongotools.GetDatabaseAndCollection(S, opts.Database, opts.Collection)
		assertNoErr(err)
	}
	// shard the collection
	c := make(chan struct{})
	done := make(chan struct{})
	go progressMeter(c, done)
	err = shardCollection(S, opts.Database, opts.Collection, opts.Key)
	// shut down the progress meter
	close(done)
	<-c
	// exit, with an error if we failed
	assertNoErr(err)
}

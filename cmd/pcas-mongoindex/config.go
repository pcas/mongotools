// Config handles flag and argument processing for pcas-mongoindex

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"fmt"
	"github.com/pkg/errors"
	"os"
)

// Options describes the options.
type Options struct {
	Unique bool // whether only a unique document with a given index should be allowed
	Sparse bool // whether to index only documents containing the key field
	Create bool // whether to create the collection, if it does not exist

	Drop bool // whether to drop the index (true) or create it (false)

	Address *address.Address // the address of the MongoDB server

	Database   string // the name of the MongoDB database
	Collection string // the name of the MongoDB collection
	Key        string // the name of the field to index
}

// Name is the name of the executable.
const Name = "pcas-mongoindex"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions parses and validates configuration information and command-line arguments, returning the options specified thereby.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse and validate the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// prepare the default address
	addr, err := address.NewTCP("localhost", 27017)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // this can't happen
	}
	// return the defaults
	return &Options{
		Unique:  false,
		Sparse:  false,
		Create:  false,
		Drop:    false,
		Address: addr,
	}
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf(`%s lists indices on, adds new indices to, or deletes indices from MongoDB collections.

Usage: %s [options] [name]

Here name is the name of the field to index.  If name is omitted, existing indices are listed.`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address of the MongoDB server, in the form hostname:port", ""),
		flag.String("collection", &opts.Collection, opts.Collection, "The MongoDB collection", ""),
		flag.Bool("create", &opts.Create, opts.Create, "Whether to create the collection, if it does not exist", ""),
		flag.String("database", &opts.Database, opts.Database, "The MongoDB database", ""),
		flag.Bool("drop", &opts.Drop, opts.Drop, "Whether to drop the index (true) or create it (false)", ""),
		flag.Bool("sparse", &opts.Sparse, opts.Sparse, "Whether to index only documents containing the key field", ""),
		flag.Bool("unique", &opts.Unique, opts.Unique, "Whether to enforce that documents with a given value of the key field must be unique", ""),
		&version.Flag{AppName: Name},
	)
	// Parse the flags
	flag.Parse()
	// The remaining argument, if present, should be the key to index on
	opts.Key = flag.Arg(0)
	if len(flag.Args()) > 1 {
		return errors.New("Too many arguments")
	}
	// Sanity check
	if opts.Address.Port() <= 0 || opts.Address.Port() > 65535 {
		return errors.New("The port number must be in the range 1-65535")
	}
	return nil
}

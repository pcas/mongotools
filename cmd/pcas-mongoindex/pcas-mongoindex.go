// Pcas-mongoindex lists indices on or adds new indices to MongoDB collections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/mongotools"
	"fmt"
	"gopkg.in/mgo.v2"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// progressMeter displays a progress meter.  To stop this, close done.  progress_meter closes c just before it exits.  Designed to be run in its own goroutine.
func progressMeter(c chan struct{}, done chan struct{}) {
	fmt.Printf("Building index")
	ticker := time.NewTicker(1 * time.Second)
	i := 0
	ok := true
	for ok {
		select {
		case <-ticker.C:
			if i == 3 {
				fmt.Printf("\b\b\b")
				i = 0
			} else {
				fmt.Printf(".")
				i++
			}
		case <-done:
			ok = false
			for j := i; j < 3; j++ {
				fmt.Printf(".")
			}
			fmt.Printf("done\n")
		}
	}
	// Close c to signal that we are exiting, and then exit
	close(c)
	return
}

// printIndex pretty-prints the given index.
func printIndex(idx mgo.Index) {
	fmt.Printf("Key: %v\n", idx.Key)
	fmt.Printf("- Unique: %v\n", idx.Unique)
	fmt.Printf("- DropDups: %v\n", idx.DropDups)
	fmt.Printf("- Background: %v\n", idx.Background)
	fmt.Printf("- Sparse: %v\n", idx.Sparse)
	return
}

// printIndices prints the indices in the collection coll.
func printIndices(coll *mgo.Collection) error {
	indexes, err := coll.Indexes()
	if err != nil {
		return err
	}
	fmt.Printf("Existing indices:\n")
	for _, x := range indexes {
		printIndex(x)
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// main
func main() {
	// Parse the options
	opts := setOptions()
	// Dial the MongoDB server
	S, err := mgo.DialWithTimeout(opts.Address.URI(), 10*time.Second)
	assertNoErr(err)
	// Grab the collection
	var coll *mgo.Collection
	if opts.Create {
		coll = S.DB(opts.Database).C(opts.Collection)
	} else {
		_, coll, err = mongotools.GetDatabaseAndCollection(S, opts.Database, opts.Collection)
		assertNoErr(err)
	}
	// If no key was provided, we list the indices and exit
	if opts.Key == "" {
		assertNoErr(printIndices(coll))
		return
	}
	// If we were asked to drop the index, then drop it
	if opts.Drop {
		assertNoErr(coll.DropIndex(opts.Key))
		return
	}
	// build the index
	c := make(chan struct{})
	done := make(chan struct{})
	go progressMeter(c, done)
	err = coll.EnsureIndex(mgo.Index{
		Key:        []string{opts.Key},
		Unique:     opts.Unique,
		DropDups:   false,
		Background: false,
		Sparse:     opts.Sparse,
	})
	// shut down the progress meter
	close(done)
	<-c
	// exit, with an error if we failed
	assertNoErr(err)
}

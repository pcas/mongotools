// Mongo handles database communication for pcas-mongosentry

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/mongotools"
	"bitbucket.org/pcastools/log"
	"context"
	"github.com/gonum/stat"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"math"
	"reflect"
	"time"
)

// replicationInfo holds fields that we examine in the results of calling the mongo command collStats on the oplog.
type replicationInfo struct {
	Size    int `bson:"size"`
	MaxSize int `bson:"maxSize"`
}

// optime holds fields that we examine in the results of calling the mongo command replSetGetStatus.
type optime struct {
	Ts bson.MongoTimestamp `bson:"ts"`
}

// memberInfo holds fields that we examine in the results of calling the mongo command replSetGetStatus.
type memberInfo struct {
	Optime *optime `bson:"optime"`
}

// replSetStatus holds fields that we examine in the results of calling the mongo command replSetGetStatus.
type replSetStatus struct {
	Date    time.Time     `bson:"date"`
	Members []*memberInfo `bson:"members"`
}

// dbList holds the fields that we examine in the results of calling the mongo command "listDatabases".
type dbList struct {
	Databases []struct {
		Name   string         `bson:"name"`
		Shards map[string]int `bson:"shards"`
	} `bson:"databases"`
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// toNearestSecond converts s to a time.Time, to second precision only.
func toNearestSecond(s bson.MongoTimestamp) time.Time {
	return time.Unix(int64(s)/(1<<32), 0)
}

// makeFields populates fields with key-value pairs where the keys are indicated by 'field' struct tags in X and the values are the corresponding struct fields in X.
func makeFields(X interface{}, fields metrics.Fields) error {
	// sanity check: X must be a struct
	t := reflect.TypeOf(X)
	if t.Kind() != reflect.Struct {
		return errors.New("Argument must be a struct")
	}
	// walk the struct fields
	s := reflect.ValueOf(X)
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		// if this field is a struct, then recurse
		if f.Type().Kind() == reflect.Struct {
			makeFields(f.Interface(), fields)
		}
		// does this have a struct tag 'field'?
		if k, ok := t.Field(i).Tag.Lookup("field"); ok {
			fields[k] = f.Interface()
		}
	}
	return nil
}

// pointFromDbStats turns d into a metrics.Point, using the fields specified by the struct tags 'field' in d.
func pointFromDbStats(d dbStats) (*metrics.Point, error) {
	fields := metrics.Fields{}
	makeFields(d, fields)
	if d.FsTotalSize != 0 {
		fields["fsPercentFull"] = 100.0 * d.FsUsedSize / d.FsTotalSize
	}
	return metrics.NewPoint("mongoDB.dbstats", fields, nil)
}

// pointFromServerStatus turns s into a metrics.Point, using the fields specified by the struct tags 'field' in s.
func pointFromServerStatus(s serverStatus) (*metrics.Point, error) {
	fields := metrics.Fields{}
	makeFields(s, fields)
	return metrics.NewPoint("mongoDB.serverStatus", fields, nil)
}

// submitServerStatus submits metrics information about the MongoDB server represented by S to the metrics endpoint met.
func submitServerStatus(ctx context.Context, S *mgo.Session, met metrics.Interface) error {
	var serverSt serverStatus
	var p *metrics.Point
	var err error
	if err = mongotools.RunCommandAndUnmarshal(S, "admin", "serverStatus", &serverSt); err != nil {
		return err
	} else if p, err = pointFromServerStatus(serverSt); err != nil {
		return err
	} else if err = met.Submit(ctx, p); err != nil {
		return err
	}
	// We succeeded
	return nil
}

// submitDatabaseStatus submits metrics information about the database db on the server represented by S to the metrics endpoint met.
func submitDatabaseStatus(ctx context.Context, S *mgo.Session, db string, met metrics.Interface) error {
	var dbSt dbStats
	var p *metrics.Point
	var err error
	if err = mongotools.RunCommandAndUnmarshal(S, db, "dbStats", &dbSt); err != nil {
		return err
	} else if p, err = pointFromDbStats(dbSt); err != nil {
		return err
	} else if err = met.Submit(ctx, p); err != nil {
		return err
	}
	// We succeeded
	return nil
}

// getOplogWindow gets the oplog window in seconds.  localDB is the database "local".
func getOplogWindow(localDB *mgo.Database) (time.Duration, error) {
	// grab the names of collections in "local"
	collections, err := localDB.CollectionNames()
	if err != nil {
		return 0, err
	}
	// grab the oplog
	success := false
	for _, c := range collections {
		if c == "oplog.rs" {
			success = true
		}
	}
	if !success {
		return 0, errors.New("Missing oplog.  Is this really a replica set?")
	}
	oplog := localDB.C("oplog.rs")
	// grab the timestamps of the first and last entries in the oplog
	first := bson.M{}
	last := bson.M{}
	if err := oplog.Find(nil).Sort("$natural").One(&first); err != nil {
		return 0, err
	}
	if err := oplog.Find(nil).Sort("-$natural").One(&last); err != nil {
		return 0, err
	}
	// compute the oplog window and return
	firstS := toNearestSecond(first["ts"].(bson.MongoTimestamp))
	lastS := toNearestSecond(last["ts"].(bson.MongoTimestamp))
	return lastS.Sub(firstS), nil
}

// getOplogSizes returns the actual and maximum sizes of the oplog, in MB.  localDB is the database "local".
func getOplogSizes(localDB *mgo.Database) (actual float64, max float64, err error) {
	var r replicationInfo
	err = localDB.Run(bson.M{"collStats": "oplog.rs"}, &r)
	if err != nil {
		return
	}
	actual = float64(r.Size) / (1024 * 1024)
	max = float64(r.MaxSize) / (1024 * 1024)
	return
}

// getReplicationLag returns the maximum replication lag in the replica set represented by S.
func getReplicationLag(S *mgo.Session) (time.Duration, error) {
	// run the MongoDB command "replSetGetStatus" and extract the results
	var rSt replSetStatus
	if err := mongotools.RunCommandAndUnmarshal(S, "admin", "replSetGetStatus", &rSt); err != nil {
		return 0, err
	}
	// find the earliest reported optime
	var earliest time.Time
	for _, x := range rSt.Members {
		t := toNearestSecond(x.Optime.Ts)
		if earliest.IsZero() || earliest.After(t) {
			earliest = t
		}
	}
	// return the maximum replication lag
	return rSt.Date.Sub(earliest), nil
}

// submitReplicationStatus submits metrics information about replication on the replica set represented by S to the metrics endpoint met.
func submitReplicationStatus(ctx context.Context, S *mgo.Session, met metrics.Interface) error {
	// grab the local database
	localDB := S.DB("local")
	// get the oplog window
	oplogWindow, err := getOplogWindow(localDB)
	if err != nil {
		return err
	}
	// extract the oplog size and maximum size
	usedMB, logSizeMB, err := getOplogSizes(localDB)
	if err != nil {
		return err
	}
	// get the replication lag
	replicationLag, err := getReplicationLag(S)
	if err != nil {
		return err
	}
	// assemble the fields to report
	fields := metrics.Fields{
		"usedMB":         usedMB,
		"logSizeMB":      logSizeMB,
		"oplogWindow":    oplogWindow,
		"replicationLag": replicationLag,
	}
	p, err := metrics.NewPoint("mongoDB.replicationInfo", fields, nil)
	if err != nil {
		return err
	}
	return met.Submit(ctx, p)
}

// dbNamesAndShardBalance returns the parallel slices consisting of: the names of the databases on the MongoDB server represented by S and, if S is sharded, the means and variances for that database of the shard sizes in Mb.  If S is not sharded then means and variances are nil.
func dbNamesAndShardBalance(S *mgo.Session) (names []string, means []float64, variances []float64, err error) {
	var dbL dbList
	if err = mongotools.RunCommandAndUnmarshal(S, "admin", "listDatabases", &dbL); err != nil {
		return
	}
	// copy the names, and check if we ever see any shards
	names = make([]string, 0, len(dbL.Databases))
	hasShards := false
	for _, x := range dbL.Databases {
		names = append(names, x.Name)
		if len(x.Shards) != 0 {
			hasShards = true
		}
	}
	// if there are no shards, return what we have
	if !hasShards {
		return
	}
	// compute the means and variances
	means = make([]float64, 0, len(names))
	variances = make([]float64, 0, len(names))
	for _, x := range dbL.Databases {
		values := make([]float64, 0, len(x.Shards))
		for _, v := range x.Shards {
			// we convert to Mb
			values = append(values, float64(v)/(1024*1024))
		}
		m, v := stat.MeanVariance(values, nil)
		means = append(means, m)
		variances = append(variances, v)
	}
	return
}

// submitShardBalance submits a metrics point with name "mongoDB.shardBalance" and fields "mean" and "variance", with the specified values, to the metrics endpoint met
func submitShardBalance(ctx context.Context, mean float64, variance float64, met metrics.Interface) error {
	fields := metrics.Fields{}
	if !math.IsNaN(mean) && !math.IsNaN(variance) {
		fields = metrics.Fields{
			"mean":     mean,
			"variance": variance,
		}
	}
	p, err := metrics.NewPoint("mongoDB.shardBalance", fields, nil)
	if err != nil {
		return err
	}
	return met.Submit(ctx, p)
}

/////////////////////////////////////////////////////////////////////////
// Global functions
/////////////////////////////////////////////////////////////////////////

// checkReplicaSet returns true if S represents a connection to a MongoDB server that is part of a replica set.  If an error occurs, checkReplicaSet returns false. It logs to lg.
func checkReplicaSet(S *mgo.Session, lg log.Interface) bool {
	lg = log.PrefixWith(lg, "checkReplicaSet")
	// grab the local database
	localDB := S.DB("local")
	collections, err := localDB.CollectionNames()
	if err != nil {
		lg.Printf("Error: %v", err)
		return false
	}
	// check if there is an oplog
	for _, c := range collections {
		if c == "oplog.rs" {
			lg.Printf("This is a member of a replica set")
			return true
		}
	}
	lg.Printf("This is not a member of a replica set")
	return false
}

// writeMetrics writes metrics information for the mongo server S to the metrics endpoint met.  If isReplicaSet is true then additional metrics about replication are collected.
func writeMetrics(ctx context.Context, S *mgo.Session, isReplicaSet bool, met metrics.Interface) error {
	// get server-wide statistics
	if err := submitServerStatus(ctx, S, met); err != nil {
		return errors.Wrapf(err, "Error getting server statistics")
	}
	// for each database on S, get database statistics
	dbs, means, variances, err := dbNamesAndShardBalance(S)
	if err != nil {
		return err
	}
	for i, db := range dbs {
		if err := submitDatabaseStatus(ctx, S, db, metrics.TagWith(met, metrics.Tags{"Database": db})); err != nil {
			return errors.Wrapf(err, "Error getting database statistics for database %s", db)
		}
		if means != nil && variances != nil {
			if err := submitShardBalance(ctx, means[i], variances[i], metrics.TagWith(met, metrics.Tags{"Database": db})); err != nil {
				return errors.Wrapf(err, "Error submitting shard balance for database %s", db)
			}
		}
	}
	// if appropriate, get replication statistics
	if isReplicaSet {
		if err := submitReplicationStatus(ctx, S, met); err != nil {
			return errors.Wrap(err, "Error getting replication statistics")
		}
	}
	// we succeeded
	return nil
}

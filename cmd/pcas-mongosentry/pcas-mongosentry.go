// Pcas-mongosentry monitors running MongoDB processes (mongod and/or mongos) and sends performance metrics to a pcas metrics endpoint.  It obtains metrics by running database commands directly and extracting certain fields from the results; to collect additional fields from these database commands, edit json.go

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"gopkg.in/mgo.v2"
	"sync"
	"time"
)

// InitialRedialTimeout is the timeout between server redials when an error occurs.  We double it after each error, up to a maximum value of MaxDialTimeout.
const InitialRedialTimeout = 30 * time.Second

// MaxRedialTimeout is the maximum timeout between server redials when an error occurs.
const MaxRedialTimeout = 10 * time.Minute

// MongoServer records the Url of a MongoDB server, and how often to query it
type MongoServer struct {
	URL   string        // the URL of the server
	Pulse time.Duration // how often to query the server for performance metrics
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// watch monitors the specified MongoDB server, writing metrics information to met every pulse seconds. It can be cancelled by the context ctx.  If isReplicaSet is true then additional metrics about replication are collected.
func watch(ctx context.Context, S *mgo.Session, pulse time.Duration, isReplicaSet bool, met metrics.Interface) error {
	for {
		t := time.NewTimer(pulse)
		select {
		case <-t.C:
			if err := writeMetrics(ctx, S, isReplicaSet, met); err != nil {
				return err
			}
		case <-ctx.Done():
			// Return nil, signifying that we were explicitly cancelled
			return nil
		}
	}
}

// monitor monitors the specified server, writing metrics information to the metrics endpoint met and logging to lg.  It shuts down when ctx is cancelled, decrementing the WaitGroup wg.
func monitor(ctx context.Context, s MongoServer, wg *sync.WaitGroup, met metrics.Interface, lg log.Interface) {
	var S *mgo.Session
	var err error
	// isReplicaSet is true if and only if s represents a server that is a member of a replica set
	var isReplicaSet bool
	// we loop, redialling on error and shutting down when ctx is cancelled
	redialTimeout := InitialRedialTimeout
	shutdown := false
	for !shutdown {
		if S == nil {
			lg.Printf("Dialling %v with pulse %v", s.URL, s.Pulse)
			S, err = mgo.DialWithTimeout(s.URL, redialTimeout)
			if err != nil {
				lg.Printf("Error when dialling: %v", err)
				time.Sleep(redialTimeout)
				if redialTimeout *= 2; redialTimeout > MaxRedialTimeout {
					redialTimeout = MaxRedialTimeout
				}
				S = nil
			} else {
				lg.Printf("Dialled successfully")
				redialTimeout = InitialRedialTimeout
				isReplicaSet = checkReplicaSet(S, lg)
			}
		} else {
			err := watch(ctx, S, s.Pulse, isReplicaSet, met)
			if err != nil {
				lg.Printf("Error when monitoring: %v", err)
				// try to disconnect from S cleanly
				go S.Close()
				// ensure that we redial, after a timeout
				S = nil
				time.Sleep(redialTimeout)
			} else {
				// We returned without error, so we must have been cancelled by the context
				shutdown = true
				S.Close()
				lg.Printf("Exiting monitor")
			}
		}
	}
	wg.Done()
	return
}

func main() {
	// run cleanup functions on exit
	defer cleanup.Run()
	// process flags and command-line arguments
	servers, err := setOptions()
	assertNoErr(err)
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Launch the monitors
	var wg sync.WaitGroup
	for _, s := range servers {
		wg.Add(1)
		go monitor(ctx, s, &wg, metrics.TagWith(metrics.Metrics(), metrics.Tags{"URL": s.URL}), log.Log())
	}
	// Wait for the monitors to exit
	wg.Wait()
}

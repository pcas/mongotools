// Config handles flag and argument processing for pcas-mongosentry

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"context"
	"fmt"
	"os"
	"time"
)

// Options describes the options.
type Options struct {
	Pulse time.Duration // The interval at which to report metrics data
}

// Name is the name of the executable.
const Name = "pcas-mongosentry"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions parses and validates configuration information and command-line arguments, returning the slice of MongoDB servers to monitor.
func setOptions() ([]MongoServer, error) {
	// Create the default values
	opts := defaultOptions()
	// Parse and validate the configuration information
	return parseArgs(opts)
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		Pulse: 5 * time.Minute,
	}
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// setMetrics starts the metrics reporting.
func setMetrics(sslClientSet *sslflag.ClientSet, metricsdbSet *metricsdbflag.MetricsSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Create the config
	c := metricsdbSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the metrics
	return metricsdbflag.SetMetrics(ctx, c, Name)
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) ([]MongoServer, error) {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf(`%s monitors MongoDB servers and writes performance metrics to a specified pcas metrics endpoint.

Usage: %s [options] url1 url2 ... urlN

Here url1, ... , urlN are URLs specifying a MongoDB server.  Each of them should take the form hostname:port.`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Duration("pulse", &opts.Pulse, opts.Pulse, "The interval at which to report metrics data", ""),
	)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Define the usage message.
	flag.SetGlobalFooter(`pcas-mongosentry obtains metrics by running database commands directly and extracting certain fields from the results.  To collect additional fields from these database commands, edit json.go and recompile.

mongoDB.dbstats

For each database on the server, a metrics point with the name "mongoDB.dbstats" is recorded, tagged with the database name (as tag "Database") and the URL of the MongoDB server (as tag "URL").  It has the following fields

avgObjSize
collections
dataSize
indexes
indexSize
objects
storageSize

which have the same meanings as the corresponding fields in [https://docs.mongodb.com/manual/reference/command/dbStats/#output]. It also has an additional field

fsPercentFull

which records the percentage of the filesystem containing the database that is full.

mongoDB.serverStatus

In addition, for each server, a metrics point with the name "mongoDB.serverStatus" is recorded, tagged with the URL of the MongoDB server (as tag "URL").  It has the following fields

opcounters.query
opcounters.getmore
opcounters.insert
opcounters.update
opcounters.delete
globalLock.activeClients.readers
globalLock.activeClients.writers
globalLock.currentQueue.readers
globalLock.currentQueue.writers
wiredTiger.concurrentTransactions.read.out
wiredTiger.concurrentTransactions.read.available
wiredTiger.concurrentTransactions.write.out
wiredTiger.concurrentTransactions.write.available
wiredTiger.cache.bytes_currently_in_the_cache
maximum_bytes_configured
wiredTiger.cache.tracked_dirty_bytes_in_the_cache
wiredTiger.cache.unmodified_pages_evicted
wiredTiger.cache.modified_pages_evicted
metrics.cursor.open.total
metrics.cursor.open.noTimeout
metrics.cursor.timedOut
connections.current
connections.available
mem.virtual
mem.resident
extra_info.page_faults
asserts.msg
asserts.warning
asserts.regular
asserts.user

which have the same meanings as the corresponding fields in [https://docs.mongodb.com/manual/reference/command/serverStatus/#output]. If the server is a mongos then the following additional fields are reported

shardingStatistics.countStaleConfigErrors
shardingStatistics.countDonorMoveChunkStarted
shardingStatistics.totalDonorChunkCloneTimeMillis
shardingStatistics.totalCriticalSectionTimeMillis
shardingStatistics.countIncrementalRefreshesStarted
shardingStatistics.catalogCache.numActiveIncrementalRefreshes
shardingStatistics.catalogCache.countFullRefreshesStarted
shardingStatistics.catalogCache.countFailedRefreshes

which, again, have the same meanings as the corresponding fields in [https://docs.mongodb.com/manual/reference/command/serverStatus/#output]

mongoDB.shardBalance

If the target MongoDB server is sharded then for each database on the server, a metrics point with the name "mongoDB.shardBalance" is recorded, tagged with the database name (as tag "Database") and the URL of the MongoDB server (as tag "URL").  It has the following fields

mean
variance

These record, respectively, the mean and variance of the size of the data (in Mb) from that database on each shard.`)
	// Parse the flags
	flag.Parse()
	// Set the loggers
	if err := setLoggers(sslClientSet, logdSet); err != nil {
		return nil, err
	}
	// Set the metrics
	if metricsdbSet.WithMetrics() {
		if err := setMetrics(sslClientSet, metricsdbSet); err != nil {
			return nil, err
		}
	}
	// Parse the remaining command-line arguments, which should all be URLs of the form hostname:port
	servers := make([]MongoServer, 0, len(flag.Args()))
	for _, arg := range flag.Args() {
		servers = append(servers, MongoServer{URL: arg, Pulse: opts.Pulse})
	}
	// Return the servers
	return servers, nil
}

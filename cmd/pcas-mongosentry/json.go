// Json defines the JSON fields (or in fact BSON fields, BSON being MongoDBs binary JSON format) that we collect data from, and the metrics fields in which we store these data.  To collect additional fields, add additional struct entries (with "bson" and "field" struct tags) in the appropriate struct below.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

// dbStats holds the fields that we examine in the results of the mongo shell command db.stats().  See [https://docs.mongodb.com/manual/reference/command/dbStats/#output] for the meanings of these fields.  The struct tag `field` defines the field name that we use when converting to a *metrics.Point
type dbStats struct {
	AvgObjSize  int     `bson:"avgObjSize" field:"avgObjSize"`
	Collections int     `bson:"collections" field:"collections"`
	DataSize    int     `bson:"dataSize" field:"dataSize"`
	FsUsedSize  float64 `bson:"fsUsedSize"`
	FsTotalSize float64 `bson:"fsTotalSize"`
	Indexes     int     `bson:"indexes" field:"indexes"`
	IndexSize   int     `bson:"indexSize" field:"indexSize"`
	Objects     int     `bson:"objects" field:"objects"`
	StorageSize int     `bson:"storageSize" field:"storageSize"`
}

// serverStatus holds the fields that we examine in the results of the mongo shell command db.serverStatus().  See [https://docs.mongodb.com/manual/reference/command/serverStatus/#output] for the meanings of these fields.  The struct tag `field` defines the field name that we use when converting to a *metrics.Point
type serverStatus = struct {
	OpCounters struct {
		Query   int `bson:"query" field:"opcounters.query"`
		GetMore int `bson:"getmore" field:"opcounters.getmore"`
		Insert  int `bson:"insert" field:"opcounters.insert"`
		Update  int `bson:"update" field:"opcounters.update"`
		Delete  int `bson:"delete" field:"opcounters.delete"`
	} `bson:"opcounters"`

	GlobalLock struct {
		ActiveClients struct {
			Readers int `bson:"readers" field:"globalLock.activeClients.readers"`
			Writers int `bson:"writers" field:"globalLock.activeClients.writers"`
		} `bson:"activeClients"`
		CurrentQueue struct {
			Readers int `bson:"readers" field:"globalLock.currentQueue.readers"`
			Writers int `bson:"writers" field:"globalLock.currentQueue.writers"`
		} `bson:"currentQueue"`
	} `bson:"globalLock"`

	WiredTiger struct {
		ConcurrentTransactions struct {
			Read struct {
				Out       int `bson:"out" field:"wiredTiger.concurrentTransactions.read.out"`
				Available int `bson:"available" field:"wiredTiger.concurrentTransactions.read.available"`
			} `bson:"read"`
			Write struct {
				Out       int `bson:"out" field:"wiredTiger.concurrentTransactions.write.out"`
				Available int `bson:"available" field:"wiredTiger.concurrentTransactions.write.available"`
			} `bson:"write"`
		} `bson:"concurrentTransactions"`
		Cache struct {
			Bytes             int `bson:"bytes currently in the cache" field:"wiredTiger.cache.bytes_currently_in_the_cache"`
			MaxBytes          int `bson:"maximum bytes configured" field:"maximum_bytes_configured"`
			TrackedDirtyBytes int `bson:"tracked dirty bytes in the cache" field:"wiredTiger.cache.tracked_dirty_bytes_in_the_cache"`
			UnmodifiedEvicted int `bson:"unmodified pages evicted" field:"wiredTiger.cache.unmodified_pages_evicted"`
			ModifiedEvicted   int `bson:"modified pages evicted" field:"wiredTiger.cache.modified_pages_evicted"`
		} `bson:"cache"`
	} `bson:"wiredTiger"`

	Metrics struct {
		Cursor struct {
			Open struct {
				Total     int `bson:"total" field:"metrics.cursor.open.total"`
				NoTimeout int `bson:"noTimeout" field:"metrics.cursor.open.noTimeout"`
			} `bson:"open"`
			TimedOut int `bson:"timedOut" field:"metrics.cursor.timedOut"`
		} `bson:"cursor"`
	} `bson:"metrics"`

	Connections struct {
		Current   int `bson:"current" field:"connections.current"`
		Available int `bson:"available" field:"connections.available"`
	} `bson:"connections"`

	Mem struct {
		Virtual  int `bson:"virtual" field:"mem.virtual"`
		Resident int `bson:"resident" field:"mem.resident"`
	} `bson:"mem"`

	ExtraInfo struct {
		PageFaults int `bson:"page_faults" field:"extra_info.page_faults"`
	} `bson:"extra_info"`

	Asserts struct {
		Msg     int `bson:"msg" field:"asserts.msg"`
		Warning int `bson:"warning" field:"asserts.warning"`
		Regular int `bson:"regular" field:"asserts.regular"`
		User    int `bson:"user" field:"asserts.user"`
	} `bson:"asserts"`

	ShardingStatistics *shardingStatistics `bson:"shardingStatistics"`
}

// shardingStatistics holds the sharding-related fields that we examine in the results of the mongo shell command db.serverStatus().  See [https://docs.mongodb.com/manual/reference/command/serverStatus/#output] for the meanings of these fields.  The struct tag `field` defines the field name that we use when converting to a *metrics.Point
type shardingStatistics struct {
	CountStaleConfigErrors           int `bson:"countStaleConfigErrors" field:"shardingStatistics.countStaleConfigErrors"`
	CountDonorMoveChunkStarted       int `bson:"countDonorMoveChunkStarted" field:"shardingStatistics.countDonorMoveChunkStarted"`
	TotalDonorChunkCloneTimeMillis   int `bson:"totalDonorChunkCloneTimeMillis" field:"shardingStatistics.totalDonorChunkCloneTimeMillis"`
	TotalCriticalSectionTimeMillis   int `bson:"totalCriticalSectionTimeMillis" field:"shardingStatistics.totalCriticalSectionTimeMillis"`
	CountIncrementalRefreshesStarted int `bson:"countIncrementalRefreshesStarted" field:"shardingStatistics.countIncrementalRefreshesStarted"`
	CatalogCache                     struct {
		NumActiveFullRefreshes    int `bson:"numActiveFullRefreshes" field:"shardingStatistics.catalogCache.numActiveIncrementalRefreshes"`
		CountFullRefreshesStarted int `bson:"countFullRefreshesStarted" field:"shardingStatistics.catalogCache.countFullRefreshesStarted"`
		CountFailedRefreshes      int `bson:"countFailedRefreshes" field:"shardingStatistics.catalogCache.countFailedRefreshes"`
	} `bson:"catalogCache"`
}

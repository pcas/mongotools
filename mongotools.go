// Mongotools provides tools for working with MongoDB databases via the mgo driver

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongotools

import (
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
)

// GetDatabase returns the specified database, or an error if the database is not present.
func GetDatabase(S *mgo.Session, database string) (*mgo.Database, error) {
	names, err := S.DatabaseNames()
	if err != nil {
		return nil, err
	}
	for _, x := range names {
		if x == database {
			return S.DB(database), nil
		}
	}
	return nil, errors.New("Unknown database")
}

// GetCollection returns the specified collection from db, or an error if the collection is not present.
func GetCollection(db *mgo.Database, collection string) (*mgo.Collection, error) {
	names, err := db.CollectionNames()
	if err != nil {
		return nil, err
	}
	for _, x := range names {
		if x == collection {
			return db.C(collection), nil
		}
	}
	return nil, errors.New("Unknown collection")
}

// GetDatabaseAndCollection returns the specified database and collection, or an error if either is not present
func GetDatabaseAndCollection(S *mgo.Session, database string, collection string) (*mgo.Database, *mgo.Collection, error) {
	if db, err := GetDatabase(S, database); err != nil {
		return nil, nil, err
	} else if coll, err := GetCollection(db, collection); err != nil {
		return nil, nil, err
	} else {
		return db, coll, nil
	}
}

// RunCommandAndUnmarshal runs the specified command on the specified database, unmarshalling the result into X.  The command can either be a string equal to the command name, or an mgo bson.M document with the full command.
func RunCommandAndUnmarshal(S *mgo.Session, database string, command interface{}, X interface{}) error {
	// grab the database
	db, err := GetDatabase(S, database)
	if err != nil {
		return err
	}
	// run the command
	return db.Run(command, X)
}

module bitbucket.org/pcas/mongotools

go 1.13

require (
	bitbucket.org/pcas/logger v0.1.43
	bitbucket.org/pcas/metrics v0.1.35
	bitbucket.org/pcas/sslflag v0.0.16
	bitbucket.org/pcastools/address v0.1.4
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/fatal v1.0.3
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/version v0.0.5
	github.com/gonum/blas v0.0.0-20181208220705-f22b278b28ac // indirect
	github.com/gonum/floats v0.0.0-20181209220543-c233463c7e82 // indirect
	github.com/gonum/integrate v0.0.0-20181209220457-a422b5c0fdf2 // indirect
	github.com/gonum/internal v0.0.0-20181124074243-f884aa714029 // indirect
	github.com/gonum/lapack v0.0.0-20181123203213-e4cdc5a0bff9 // indirect
	github.com/gonum/matrix v0.0.0-20181209220409-c518dec07be9 // indirect
	github.com/gonum/stat v0.0.0-20181125101827-41a0da705a5b
	github.com/pkg/errors v0.9.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
